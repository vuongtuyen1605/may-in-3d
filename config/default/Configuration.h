
#ifndef CONFIGURATION_H
#define CONFIGURATION_H
#define CONFIGURATION_H_VERSION 010109     //ma phien ban 

#define STRING_CONFIG_H_AUTHOR "(may in vip)"    // Chỉnh tên máy in và người sửa đổi
#define SHOW_BOOTSCREEN
#define STRING_SPLASH_LINE1 SHORT_BUILD_VERSION // hiển thị phiên bản phần mềm khi khởi động
#define STRING_SPLASH_LINE2 WEBSITE_URL         

#define SERIAL_PORT 0

#define BAUDRATE 250000    //Set tốc độ truyền giao tiếp giữa máy in 3D và máy tính

#ifndef MOTHERBOARD
  #define MOTHERBOARD BOARD_RAMPS_14_EFB            ////Chọn loại board kết nối
#endif

#define EXTRUDERS 1            // Cài đặt số lượng đầu in (Do máy dùng 1 đầu in nên đặt 1)

#define DEFAULT_NOMINAL_FILAMENT_DIA 3.0               //Cài đặt đường kính sợi nhựa in (Ở đây dùng loại nhựa 1.75mm)

#if ENABLED(MK2_MULTIPLEXER)
#endif

#if ENABLED(SWITCHING_EXTRUDER)
  #define SWITCHING_EXTRUDER_SERVO_NR 0
  #define SWITCHING_EXTRUDER_SERVO_ANGLES { 0, 90 } 
  #if EXTRUDERS > 3
    #define SWITCHING_EXTRUDER_E23_SERVO_NR 1
  #endif
#endif

#if ENABLED(SWITCHING_NOZZLE)
  #define SWITCHING_NOZZLE_SERVO_NR 0
  #define SWITCHING_NOZZLE_SERVO_ANGLES { 0, 90 } 
#endif

#if ENABLED(PARKING_EXTRUDER)
  #define PARKING_EXTRUDER_SOLENOIDS_INVERT     
  #define PARKING_EXTRUDER_SOLENOIDS_PINS_ACTIVE LOW 
  #define PARKING_EXTRUDER_SOLENOIDS_DELAY 250        
  #define PARKING_EXTRUDER_PARKING_X { -78, 184 }   
  #define PARKING_EXTRUDER_GRAB_DISTANCE 1           
  #define PARKING_EXTRUDER_SECURITY_RAISE 5      
  #define HOTEND_OFFSET_Z { 0.0, 1.3 }              
#endif

#if ENABLED(MIXING_EXTRUDER)
  #define MIXING_STEPPERS 2        
  #define MIXING_VIRTUAL_TOOLS 16 
#endif

#define POWER_SUPPLY 0

#if POWER_SUPPLY > 0

  #if ENABLED(AUTO_POWER_CONTROL)
    #define AUTO_POWER_FANS          
    #define AUTO_POWER_E_FANS
    #define AUTO_POWER_CONTROLLERFAN
    #define POWER_TIMEOUT 30
  #endif

#endif

#define TEMP_SENSOR_0 1            //Cài đặt loại cảm biến nhiệt độ đang sử dụng với đầu in
#define TEMP_SENSOR_1 0
#define TEMP_SENSOR_2 0
#define TEMP_SENSOR_3 0
#define TEMP_SENSOR_4 0
#define TEMP_SENSOR_BED 1              //Cài đặt sử dụng bàn in nhiệt
#define TEMP_SENSOR_CHAMBER 0

#define DUMMY_THERMISTOR_998_VALUE 25
#define DUMMY_THERMISTOR_999_VALUE 100

#define MAX_REDUNDANT_TEMP_SENSOR_DIFF 10


#define TEMP_RESIDENCY_TIME 10  
#define TEMP_HYSTERESIS 3       
#define TEMP_WINDOW     1      


#define TEMP_BED_RESIDENCY_TIME 10  
#define TEMP_BED_HYSTERESIS 3      
#define TEMP_BED_WINDOW     1      

#define HEATER_0_MINTEMP 5
#define HEATER_1_MINTEMP 5
#define HEATER_2_MINTEMP 5
#define HEATER_3_MINTEMP 5
#define HEATER_4_MINTEMP 5
#define BED_MINTEMP      5

#define HEATER_0_MAXTEMP 275
#define HEATER_1_MAXTEMP 275
#define HEATER_2_MAXTEMP 275
#define HEATER_3_MAXTEMP 275
#define HEATER_4_MAXTEMP 275
#define BED_MAXTEMP      150

#define PIDTEMP
#define BANG_MAX 255    
#define PID_MAX BANG_MAX 
#define PID_K1 0.95     
#if ENABLED(PIDTEMP)

  #define PID_FUNCTIONAL_RANGE 10 
  #define DEFAULT_Kp 22.2
  #define DEFAULT_Ki 1.08
  #define DEFAULT_Kd 114

#endif 

#define MAX_BED_POWER 255 
#if ENABLED(PIDTEMPBED)

  #define DEFAULT_bedKp 10.00
  #define DEFAULT_bedKi .023
  #define DEFAULT_bedKd 305.4

#endif 

#define PREVENT_COLD_EXTRUSION
#define EXTRUDE_MINTEMP 170

#define PREVENT_LENGTHY_EXTRUDE
#define EXTRUDE_MAXLENGTH 200

#define THERMAL_PROTECTION_HOTENDS 
#define THERMAL_PROTECTION_BED     

#define USE_XMIN_PLUG              //Cài đặt công tắc giới hạn hành trình trục X (X min = 0)
#define USE_YMIN_PLUG                          //Cài đặt công tắc giới hạn hành trình trục Y (Y min = 0)
#define USE_ZMIN_PLUG                      //Cài đặt công tắc giới hạn hành trình trục Z (Z min = 0)

#define ENDSTOPPULLUPS
#if DISABLED(ENDSTOPPULLUPS)

#endif

#define X_MIN_ENDSTOP_INVERTING false 
#define Y_MIN_ENDSTOP_INVERTING false 
#define Z_MIN_ENDSTOP_INVERTING false 
#define X_MAX_ENDSTOP_INVERTING false
#define Y_MAX_ENDSTOP_INVERTING false 
#define Z_MAX_ENDSTOP_INVERTING false 
#define Z_MIN_PROBE_ENDSTOP_INVERTING false 

#define DEFAULT_AXIS_STEPS_PER_UNIT   { 80, 80, 4000, 500 }             //Thiết lập số bước của động cơ bước các trục X = 80, Y = 80, Z = 3000, E0 = 100

#define DEFAULT_MAX_FEEDRATE          { 300, 300, 5, 25 }            //Thiết lập tốc độ tối đa của các trục

#define DEFAULT_MAX_ACCELERATION      { 3000, 3000, 100, 10000 }

#define DEFAULT_ACCELERATION          3000    
#define DEFAULT_RETRACT_ACCELERATION  3000   
#define DEFAULT_TRAVEL_ACCELERATION   3000   

#define DEFAULT_XJERK                 10.0
#define DEFAULT_YJERK                 10.0
#define DEFAULT_ZJERK                  0.3
#define DEFAULT_EJERK                  5.0

#define Z_MIN_PROBE_USES_Z_MIN_ENDSTOP_PIN
#if ENABLED(PROBING_HEATERS_OFF)
#endif

#define X_PROBE_OFFSET_FROM_EXTRUDER 10  
#define Y_PROBE_OFFSET_FROM_EXTRUDER 10  
#define Z_PROBE_OFFSET_FROM_EXTRUDER 0  
#define MIN_PROBE_EDGE 10
#define XY_PROBE_SPEED 8000
#define Z_PROBE_SPEED_FAST HOMING_FEEDRATE_Z
#define Z_PROBE_SPEED_SLOW (Z_PROBE_SPEED_FAST / 2)

#define Z_CLEARANCE_DEPLOY_PROBE   10 
#define Z_CLEARANCE_BETWEEN_PROBES  5 
#define Z_CLEARANCE_MULTI_PROBE     5 
#define Z_PROBE_LOW_POINT          -2 
#define Z_PROBE_OFFSET_RANGE_MIN -20
#define Z_PROBE_OFFSET_RANGE_MAX 20
#define X_ENABLE_ON 0
#define Y_ENABLE_ON 0
#define Z_ENABLE_ON 0
#define E_ENABLE_ON 0 
#define DISABLE_X false
#define DISABLE_Y false
#define DISABLE_Z false
#define DISABLE_E false 
#define DISABLE_INACTIVE_EXTRUDER true 
#define INVERT_X_DIR false
#define INVERT_Y_DIR true
#define INVERT_Z_DIR false
#define INVERT_E0_DIR false
#define INVERT_E1_DIR false
#define INVERT_E2_DIR false
#define INVERT_E3_DIR false
#define INVERT_E4_DIR false
#define X_HOME_DIR -1
#define Y_HOME_DIR -1
#define Z_HOME_DIR -1
#define X_BED_SIZE 200           //Thiết lập kích thước của bàn in theo trục X
#define Y_BED_SIZE 200           //Thiết lập kích thước của bàn in theo trục Y
#define X_MIN_POS 200               //Thiết lập chiều dài trục X
#define Y_MIN_POS 200              //Thiết lập chiều dài trục Y
#define Z_MIN_POS 0
#define X_MAX_POS X_BED_SIZE
#define Y_MAX_POS Y_BED_SIZE
#define Z_MAX_POS 200
#define MIN_SOFTWARE_ENDSTOPS
#if ENABLED(MIN_SOFTWARE_ENDSTOPS)
  #define MIN_SOFTWARE_ENDSTOP_X
  #define MIN_SOFTWARE_ENDSTOP_Y
  #define MIN_SOFTWARE_ENDSTOP_Z
#endif

#define MAX_SOFTWARE_ENDSTOPS
#if ENABLED(MAX_SOFTWARE_ENDSTOPS)
  #define MAX_SOFTWARE_ENDSTOP_X
  #define MAX_SOFTWARE_ENDSTOP_Y
  #define MAX_SOFTWARE_ENDSTOP_Z
#endif

#if ENABLED(MIN_SOFTWARE_ENDSTOPS) || ENABLED(MAX_SOFTWARE_ENDSTOPS)
#endif

#if ENABLED(FILAMENT_RUNOUT_SENSOR)
  #define NUM_RUNOUT_SENSORS   1    
  #define FIL_RUNOUT_INVERTING false 
  #define FIL_RUNOUT_PULLUP         
  #define FILAMENT_RUNOUT_SCRIPT "M600"
#endif

#if ENABLED(MESH_BED_LEVELING) || ENABLED(AUTO_BED_LEVELING_BILINEAR) || ENABLED(AUTO_BED_LEVELING_UBL)
  #define ENABLE_LEVELING_FADE_HEIGHT
  #define SEGMENT_LEVELED_MOVES
  #define LEVELED_SEGMENT_LENGTH 5.0 
  #if ENABLED(G26_MESH_VALIDATION)
    #define MESH_TEST_NOZZLE_SIZE    0.4 
    #define MESH_TEST_LAYER_HEIGHT   0.2  
    #define MESH_TEST_HOTEND_TEMP  205.0 
    #define MESH_TEST_BED_TEMP      60.0  

#endif

#if ENABLED(AUTO_BED_LEVELING_LINEAR) || ENABLED(AUTO_BED_LEVELING_BILINEAR)
  #define GRID_MAX_POINTS_X 3
  #define GRID_MAX_POINTS_Y GRID_MAX_POINTS_X

  #if ENABLED(AUTO_BED_LEVELING_BILINEAR)
    #if ENABLED(ABL_BILINEAR_SUBDIVISION)
      #define BILINEAR_SUBDIVISIONS 3
    #endif

  #endif

#elif ENABLED(AUTO_BED_LEVELING_UBL)


  #define MESH_INSET 1             
  #define GRID_MAX_POINTS_X 10     
  #define GRID_MAX_POINTS_Y GRID_MAX_POINTS_X

  #define UBL_MESH_EDIT_MOVES_Z    
  #define UBL_SAVE_ACTIVE_ON_M500   

#elif ENABLED(MESH_BED_LEVELING)


  #define MESH_INSET 10       
  #define GRID_MAX_POINTS_X 3    
  #define GRID_MAX_POINTS_Y GRID_MAX_POINTS_X

#endif 

#if ENABLED(AUTO_BED_LEVELING_3POINT) || ENABLED(AUTO_BED_LEVELING_UBL)

#endif


#if ENABLED(LCD_BED_LEVELING)
  #define MBL_Z_STEP 0.025    
  #define LCD_PROBE_Z_RANGE 4 
#endif


#if ENABLED(LEVEL_BED_CORNERS)
  #define LEVEL_CORNERS_INSET 30    
  #define LEVEL_CORNERS_Z_HOP  4.0  
#endif

#if ENABLED(Z_SAFE_HOMING)
  #define Z_SAFE_HOMING_X_POINT ((X_BED_SIZE) / 2)    
  #define Z_SAFE_HOMING_Y_POINT ((Y_BED_SIZE) / 2)    
#endif

#define HOMING_FEEDRATE_XY (50*60)
#define HOMING_FEEDRATE_Z  (4*60)


#if ENABLED(SKEW_CORRECTION)

  #define XY_DIAG_AC 282.8427124746
  #define XY_DIAG_BD 282.8427124746
  #define XY_SIDE_AD 200

  #define XY_SKEW_FACTOR 0.0

  #if ENABLED(SKEW_CORRECTION_FOR_Z)
    #define XZ_DIAG_AC 282.8427124746
    #define XZ_DIAG_BD 282.8427124746
    #define YZ_DIAG_AC 282.8427124746
    #define YZ_DIAG_BD 282.8427124746
    #define YZ_SIDE_AD 200
    #define XZ_SKEW_FACTOR 0.0
    #define YZ_SKEW_FACTOR 0.0
  #endif

#endif

#define EEPROM_CHITCHAT  

#define HOST_KEEPALIVE_FEATURE     
#define DEFAULT_KEEPALIVE_INTERVAL 2  
#define BUSY_WHILE_HEATING            

#define PREHEAT_1_TEMP_HOTEND 180
#define PREHEAT_1_TEMP_BED     70
#define PREHEAT_1_FAN_SPEED     0 

#define PREHEAT_2_TEMP_HOTEND 240
#define PREHEAT_2_TEMP_BED    110
#define PREHEAT_2_FAN_SPEED     0 

#if ENABLED(NOZZLE_PARK_FEATURE)
  #define NOZZLE_PARK_POINT { (X_MIN_POS + 10), (Y_MAX_POS - 10), 20 }
  #define NOZZLE_PARK_XY_FEEDRATE 100   
  #define NOZZLE_PARK_Z_FEEDRATE 5     
#endif

#if ENABLED(NOZZLE_CLEAN_FEATURE)
  #define NOZZLE_CLEAN_STROKES  12
  #define NOZZLE_CLEAN_TRIANGLES  3
  #define NOZZLE_CLEAN_START_POINT { 30, 30, (Z_MIN_POS + 1)}
  #define NOZZLE_CLEAN_END_POINT   {100, 60, (Z_MIN_POS + 1)}
  #define NOZZLE_CLEAN_CIRCLE_RADIUS 6.5
  #define NOZZLE_CLEAN_CIRCLE_FN 10
  #define NOZZLE_CLEAN_CIRCLE_MIDDLE NOZZLE_CLEAN_START_POINT
  #define NOZZLE_CLEAN_GOBACK
#endif

#define PRINTJOB_TIMER_AUTOSTART
#define LCD_LANGUAGE en
#define DISPLAY_CHARSET_HD44780 EN                 //Thiết lập sử dụng màn hình LCD 2004

#if ENABLED(SAV_3DGLCD)
  #define U8GLIB_SH1106
#endif

#define SOFT_PWM_SCALE 0
#if ENABLED(RGB_LED) || ENABLED(RGBW_LED)
  #define RGB_LED_R_PIN 34
  #define RGB_LED_G_PIN 43
  #define RGB_LED_B_PIN 35
  #define RGB_LED_W_PIN -1
#endif

#if ENABLED(NEOPIXEL_LED)
  #define NEOPIXEL_TYPE   NEO_GRBW 
  #define NEOPIXEL_PIN    4       
  #define NEOPIXEL_PIXELS 30       
  #define NEOPIXEL_IS_SEQUENTIAL   
  #define NEOPIXEL_BRIGHTNESS 127  
#endif

#if ENABLED(BLINKM) || ENABLED(RGB_LED) || ENABLED(RGBW_LED) || ENABLED(PCA9632) || ENABLED(NEOPIXEL_LED)
  #define PRINTER_EVENT_LEDS
#endif

#define SERVO_DELAY { 300 }

#endif 
